#include<stdio.h>

int main (){

    float fMonto, fMontoPesos;
    fflush(stdin);

    printf("Ingresa el monto en dolares: ");
    scanf("%f", &fMonto);

    fMontoPesos = (fMonto * 22.36);

    printf("%.2f USD es equivalente a %.2f MXN.", fMonto, fMontoPesos);

    fflush(stdin);
    getchar();
    return 0;

}