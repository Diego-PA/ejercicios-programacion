Inicio

    Variable eNumero : Entero
    Variable eSuma : Entero
    Variable eContador : Entero

    Escribir "Ingrese un número entre el 1 y el 50:"
    Leer eNumero
    Si (eNumero > 0 y eNumero <= 50)
        Hacer para eContador = 0 hasta que eContador < eNumero incrementado eContador en 1
		Hacer eSuma += eContador
        Fin hacer para
        Escribir "La suma de los predecesores es: ", eSuma
    Si no
        Escribir "El número ingresado no se encuentra entre el 1 y el 50"
    Fin si
    
Fin