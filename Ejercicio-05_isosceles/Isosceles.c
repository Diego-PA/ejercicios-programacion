#include<stdio.h>

int main () {

    float fBase, fLado, fPerimetro;

    printf("Ingresa la medida de la base del triangulo: ");
    scanf("%f", &fBase);

    printf("Ingresa la medida del lado del triangulo: ");
    scanf("%f", &fLado);

    if(fLado <= 0 || fBase <= 0){

        printf("La medida de un lado no puede ser menor o igual a 0.");

    } else {

        fPerimetro = fBase + (fLado * 2);
        printf("El perimetro del triangulo es de %.2f", fPerimetro);

    }

    fflush(stdin);
    getchar();
    return 0;

}