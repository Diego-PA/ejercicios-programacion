#include<stdio.h>

int main () {

    float fLado1, fLado2, fLado3, fPerimetro;

    printf("Ingresa la medida del primer lado del triangulo: ");
    scanf("%f", &fLado1);

	printf("Ingresa la medida del segundo lado del triangulo: ");
    scanf("%f", &fLado2);

    printf("Ingresa la medida del tercer lado del triangulo: ");
    scanf("%f", &fLado3);

    if(fLado1 <= 0 || fLado2 <= 0 || fLado3 <= 0){

        printf("La medida de un lado no puede ser menor o igual a 0.");

    } else {

        fPerimetro = fLado1 + fLado2 + fLado3;
        printf("El perimetro del triangulo es de %.2f", fPerimetro);

    }

    fflush(stdin);
    getchar();
    return 0;

}
