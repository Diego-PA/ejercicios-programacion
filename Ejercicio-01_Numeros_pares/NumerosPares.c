/*  Programa: NumerosPares.c
	Autor: Puebla Aldama Diego
	Fecha: 12/08/2020
	Objetivos: Elabora un programa que imprima los n�meros pares del 0 al 100.
*/

#include<stdio.h>

int main () {
	
	int numero = 0;
	
	while (numero < 100) {
		
		numero += 2;
		printf("%d\n", numero);
		
	};
	
	fflush(stdin);
	getchar();
	return 0;
	
}
